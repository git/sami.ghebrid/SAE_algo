#include "fonction.h"

int chargementAdherent(int tabAdherent[], int tabPoints[], int jour[], int mois[], int annee[], int carte[], int tphys)
{
    int adhe, points, valjour, valmois, valannee, perdue, i = 0, pos, anneeBonus = 2022;
    FILE* flot;
    flot = fopen("adherent.txt","r");
    if (flot == NULL) {
        printf("Erreur ouverture du fichier !\n");
        fclose(flot);
        return -2;
    }
    fscanf(flot,"%d%d%d%d%d%d",&adhe,&points,&valjour,&valmois,&valannee,&perdue);
    while(!feof(flot))
    {
        if(i >= tphys)
        {
            printf("tableau plein\n");
            fclose(flot);
            return -1;
        }
        pos = ajouter(tabAdherent,tabPoints,jour,mois,annee,carte,i,adhe,tphys);
        if (pos >= 0) {
            tabPoints[pos] = points + ((anneeBonus - valannee)*2);
            jour[pos] = valjour;
            mois[pos] = valmois;
            annee[pos] = valannee;
            carte[pos] = perdue;
            i = i + 1;
        }
        fscanf(flot,"%d%d%d%d%d%d",&adhe,&points,&valjour,&valmois,&valannee,&perdue);
    }
    if (i == 0) printf("Fichier vide !\n");
    fclose(flot);
    return i;
}

int chargementActivite(int tabPoint[],int tabNbEntree[], int tphys)
{
    int num, point,nbEntree, i =0;
    FILE*flot;
    flot = fopen("activite.txt","r");
    if (flot == NULL) {
        printf("Erreur ouverture du fichier !\n");
        fclose(flot);
        return -2;
    }
    fscanf(flot,"%d %d %d",&num, &point, &nbEntree);
    while(!feof(flot))
    {
   	 if(i >= tphys)
   	 {
   		 printf("tableau plein\n");
   		 fclose(flot);
   		 return -1;
   	 }
   	 tabPoint[num] = point;
   	 tabNbEntree[num] = 0;
   	 i = i + 1;
   	 fscanf(flot,"%d %d %d",&num, &point, &nbEntree);
    }
    fclose(flot);
    return tphys;
}


int recherche(int tabTri[], int val, int nb, int *trouve){
    int i;
    *trouve = 0;
    for(i = 0 ; i < nb ; i++) {
        if (val == tabTri[i]) {
            *trouve = 1;
            return i;
        }
        if (val < tabTri[i]) return i;
    }
    return nb; 
}


void decalageDroite(int tabTri[], int nb, int pos) {
    int j;
    for (j = nb ; j > pos ; j--) tabTri[j] = tabTri[j-1];

}



void decalageGauche(int tabTri[], int nb, int pos) {
    int j;
    for(j = pos ; j < nb-1 ; j++) tabTri[j] = tabTri[j+1];
}


int ajouter(int tabAdhe[], int tabPoints[], int jour[], int mois[], int annee[], int carte[], int nb, int val, int tmax) {
    int pos, trouve;
    if (nb == tmax) {
        printf("Impossible d'insérer un nouvel élément, le tableau est plein !\n");
        return -1;
    }
    pos = recherche(tabAdhe, val, nb, &trouve);
    if (trouve == 1) {
        printf("Valeur déjà présente dans le tableau !\n");
        return -1;
    }
    decalageDroite(tabAdhe, nb, pos);
    decalageDroite(tabPoints, nb, pos);
    decalageDroite(jour, nb, pos);
    decalageDroite(mois, nb, pos);
    decalageDroite(annee, nb, pos);
    decalageDroite(carte, nb, pos);
    tabAdhe[pos] = val;
    return pos;
}

int ajouterAdherent(int tabAdhe[], int tabPoints[], int jour[], int mois[], int annee[], int carte[], int nb, int tmax) {
    int val, trouve, points = 10, valJour, valMois, valAnnee, perdue = 1, pos, coderetour;
    printf("Nouveau numéro de carte : ");
    scanf("%d",&val);
    pos = recherche(tabAdhe, val, nb, &trouve);
    coderetour = ajouter(tabAdhe, tabPoints, jour, mois, annee, carte, nb, val, tmax);
    if (coderetour == -1) {
        printf("Erreur ajout du nouvel élement !\n");
        return nb;
    }
    printf("\nJour : ");
    scanf("%d",&valJour);
    printf("\nMois : ");
    scanf("%d",&valMois);
    printf("\nAnnée : ",valAnnee);
    scanf("%d",&valAnnee);
    tabPoints[pos] = points;
    jour[pos] = valJour;
    mois[pos] = valMois;
    annee[pos] = valAnnee;
    carte[pos] = perdue;
    nb += 1;
    return nb;
            
    

}


int supprimerAdherent(int tabAdhe[], int tabPoints[], int jour[], int mois[], int annee[], int carte[], int tailleL) {
    int numAdhe;
    printf("Numéro de l'adhérent à supprimer ?\n");
    scanf("%d",&numAdhe);
    tailleL=suppression(tabAdhe,tabPoints,jour,mois,annee,carte,tailleL,numAdhe);
    return tailleL;
}


int suppression(int tab[], int tabPoints[], int jour[], int mois[], int annee[], int carte[],int nb, int val) {
    int pos, trouve;
    if (nb == 0) {
        printf("Aucun élément à supprimer dans le tableau !\n");
        return nb;
    }
    pos = recherche(tab, val, nb, &trouve);
    decalageGauche(tab, nb, pos);
    decalageGauche(tabPoints, nb, pos);
    decalageGauche(jour, nb, pos);
    decalageGauche(mois, nb, pos);
    decalageGauche(annee, nb, pos);
    decalageGauche(carte,nb,pos);
    nb -= 1;
    return nb;

}


int activerCarte(int tabAdhe[], int carte[], int tailleL, int val) {
    int pos, trouve;
    pos = recherche(tabAdhe,val,tailleL,&trouve);
    if (trouve == 0) {
        printf("La carte n'existe pas !\n");
        return -1;
    }
    else {
        if (carte[pos] == 0) {
            carte[pos] = 1;
            printf("La carte a bien été activée.\n");
        }
        else {
            carte[pos] = 0;
            printf("La carte a bien été désactivée.\n");
        }
    }
    return 0;
}


void gestionCarte(int tabAdhe[], int tabPoints[], int jour[], int mois[], int annee[], int carte[], int tailleL) {
    int choix, numCarte, trouve;
    printf("Numéro de la carte : ");
    scanf("%d",&numCarte);
    recherche(tabAdhe,numCarte,tailleL,&trouve);
    if (trouve == 1) {
        while(1) {
        printf("\n1 \t\t Afficher les informations sur la carte\n");
        printf("2 \t\t Activer/Désactiver carte\n");
        printf("3 \t\t Alimenter une carte\n");
        printf("8 \t\t Changer de carte\n");
        printf("9 \t\t Quitter\n");
        printf("Numéro de la carte actuelle : %d\n",numCarte);
        scanf("%d",&choix);
        switch(choix) {
            case 1:
                system("clear");
                affichageCarte(tabAdhe,tabPoints,jour,mois,annee,carte,tailleL,numCarte);
                break;
            case 2:
                system("clear");
                activerCarte(tabAdhe,carte,tailleL,numCarte);
                break;
            case 3:
                system("clear");
                alimenterCarte(tabAdhe,tabPoints,tailleL);
                break;
            case 8:
                system("clear");
                printf("Numéro de la carte : ");
                scanf("%d",&numCarte);
                recherche(tabAdhe,numCarte,tailleL,&trouve);
                if (trouve == 1) break;
                else {
                    printf("Erreur ! Numéro de carte inexistant !\n");
                    return ;
                }
            case 9:
                system("clear");
                return ;
                
        }
    }

}
    else {
        printf("Erreur ! Numéro de carte inexistant !\n");
        return ;
    }



}


void affichageTous(int tabAdhe[], int tabPoints[], int jour[], int mois[], int annee[], int carte[],int taille)
{
    int i;
    printf("Numéro\tPoints\tDate d'activation\t\n");
    for (i = 0 ; i < taille ; i++) {
        if (carte[i] == 1) printf("%d\t%d\t%d/%d/%d\t\t Carte Active\n",tabAdhe[i],tabPoints[i],jour[i],mois[i],annee[i]);
        else if (carte[i] == 0) printf("%d\t%d\t%d/%d/%d\t\t Carte Désactivée\n",tabAdhe[i],tabPoints[i],jour[i],mois[i],annee[i]);
    }
}


void affichageCarte(int tabAdhe[], int tabPoints[], int jour[], int mois[], int annee[], int carte[], int nb, int val) {
    int pos, trouve;
    pos = recherche(tabAdhe, val, nb, &trouve);
    if (trouve == 0) printf("Valeur non trouvée !\n");
    else {
        printf("Numéro\tPoints\tDate d'activation\t\n");
        if (carte[pos] == 1) printf("%d\t%d\t%d/%d/%d\t\t Carte Active\n",tabAdhe[pos],tabPoints[pos],jour[pos],mois[pos],annee[pos]);
        else if (carte[pos] == 0) printf("%d\t%d\t%d/%d/%d\t\t Carte Désactivée\n",tabAdhe[pos],tabPoints[pos],jour[pos],mois[pos],annee[pos]);
    }
}

void affichageToutActivite(int tabCoutPoint[], int tabNbEntree[], int tailleL)
{
    int i;
    printf("N°Activité \t Points \t Nombre Entrées \n");
    for (i = 0; i < tailleL; i++)
    {
        if (tabCoutPoint[i] != 0)
            printf("%d \t\t %d \t\t %d\n", i, tabCoutPoint[i], tabNbEntree[i]);
    }
}

void affichageActivite(int tabCoutPoint[], int tabNbEntree[], int tailleL)
{
    int num;
    printf("Plus d'information de l'activité :");
    scanf("%d", &num);
    if (tabCoutPoint[num] == 0)
        printf("Le numéro d'activité n'éxiste pas\n");
    else
    {
        printf("N°Activité \t Points \t Nombre Entrées \n");
        printf("%d \t\t %d \t\t %d\n", num, tabCoutPoint[num], tabNbEntree[num]);
    }

}


void afficheNbEntreAct(int tabNbEntree[], int tabCoutPoint[], int tphys)
{
	int num;
	printf("numero d’activité :");
	scanf("%d",&num);
	if (tabCoutPoint[num] == 0 && tabNbEntree[num] == 0)
		printf("Activité inexistante\n");
	else
		printf("Nombre d’entrée : %d", tabNbEntree[num]);
	
	
}

void alimenterCarte (int tabAdherent[], int tabPoint[],int nb)
{
    int num, trouve, i;
    char formule;
    printf("numero carte :");
    scanf("%d%*c",&num);
    i = recherche(tabAdherent, num, nb, &trouve);
    if (trouve == 1)
    {
        printf("Quelle formule :\n");
        printf("A : 5€ -> 5 points\n");
        printf("B : 10€ -> 12 point\n");
        printf("C : 15€ -> 20 point\n");
        printf("Choisir :");
        scanf("%c%*c", &formule);
        while (formule != 'A' && formule != 'B' && formule != 'C')
        {
            printf("Erreur récupération de donnée\n");
            printf("A : 5€ -> 5 points\n");
            printf("B : 10€ -> 12 point\n");
            printf("C : 15€ -> 20 point\n");
            printf("Choisir :");
            scanf("%c%*c", &formule);  
        }
        if (formule == 'A')
        {
            tabPoint[i] = tabPoint[i] + 5;
            printf("accréditation de 5 points\n");
        }
        if (formule == 'B')
        {
            tabPoint[i] = tabPoint[i] + 12;
            printf("accréditation de 12 points\n");
        }
        if (formule == 'C')
        {
            tabPoint[i] = tabPoint[i] + 20;
            printf("accréditation de 20 points\n");
        }
    }
    if (trouve == 0)
        printf("Erreur ! Numéro de carte absente de la base de donnée\n");
}

void ajoutActivite(int tabCoutPoint[],int tailleL)
{
    int num, nbPoint;
    printf("Numéro de la Nouvelle Activité :");
    scanf("%d", &num);
    if (tabCoutPoint[num] != 0)
        printf("Numéro d'activité déjà existant.\n");
    if (tailleL < num)
        printf("tableau plein");
    else 
    {
        printf("Coût de l'activité :");
        scanf("%d", &nbPoint);
        printf("Récapitulatif :\n");
        printf("\tNuméro d'activité : %d\n", num);
        printf("\tNombre de point : %d\n", nbPoint);
        tabCoutPoint[num] = nbPoint;
    }

}

void suppActivite(int tabCoutPoint[], int tabNbEntree[],int tailleL)
{
    int num;
    char choix;
    printf("Quel Activité voulez vous supprimer : ");
    scanf("%d%*c", &num);
    if (tabCoutPoint[num] == 0)
        printf("Numéro d'activité pas existant\n");
    if (tailleL < num)
        printf("tableau plein\n");
    if (tabCoutPoint[num] != 0)
    {
        printf("Êtes vous sûre de vouloir supprimer l'activité (O|N): ");
        scanf("%c%*c", &choix);
        while(choix != 'N' && choix != 'O')
        {
            printf("Êtes vous sûre de vouloir supprimer l'activité (O|N): ");
            scanf("%c%*c", &choix);
        }
        if (choix == 'N')
            printf("Annulation de la suppression\n");
        if (choix == 'O')
        {
            printf("Suppression Activité %d\n", num);
            tabCoutPoint[num] = 0;
            tabNbEntree[num] = 0;
        }
    }

}

void modifActivite(int tabCoutPoint[],int tailleL )
{
    int num, newCout;
    printf("Quel Activité voulez vous modifier : ");
    scanf("%d%*c", &num);
    if (tabCoutPoint[num] == 0)
        printf("Numéro d'activité pas existant\n");
    if (tailleL < num)
        printf("tableau plein\n");
    if (tabCoutPoint[num] != 0)
    {
        printf("Anciens coût de l'activité : %d\n", tabCoutPoint[num]);
        printf("Nouveau coût de l'activité: ");
        scanf("%d", &newCout);
        while(newCout <= 0)
        {
            printf("votre activite doit valoir plus de 0 points\n");
            printf("Nouveau coût de l'activité: ");
            scanf("%d", &newCout);
        }
        tabCoutPoint[num] = newCout;
        printf("L'activité coûte désormais %d", tabCoutPoint[num]);
    }
}

void faireActivite(int tabAdherent[], int tabPoint[],int tabCoutPoint[], int tabNbEntree[], int tailleLAdhe, int tailleLAct)
{
    int num, idAct, i, trouve;
    printf("Identifiant Adhérent : ");
    scanf("%d", &num);
    i = recherche(tabAdherent, num, tailleLAdhe, &trouve);
    if (trouve == 0)
        printf("Identifiant Adhérent pas existant\n");
    else
    {
        printf("Numéro d'activité que vous voulez pratiquer : ");
        scanf("%d", &idAct);
        while (tabCoutPoint[idAct] == 0)
        {
            printf("Activité inexistante\n");
            printf("Numéro d'activité que vous voulez pratiquer : ");
            scanf("%d", &idAct);
            if (idAct == -1)
            {
                printf("annulation réservation Activité !\n");
                exit(1); 
            }
        }
        if (tabCoutPoint[idAct] > tabPoint[i])
            printf("Vous n'avez pas assez de point");
        else{
            tabPoint[i] = tabPoint[i] - tabCoutPoint[idAct];
            tabNbEntree[idAct] = tabNbEntree[idAct] + 1;
            printf("Achat effectué\n");
            printf("Il vous reste %d point sur votre carte\n", tabPoint[i]);
        }

    }
    
}

int SauvegardeAdherent(int tabAdherent[], int tabPoints[], int jour[], int mois[], int annee[], int carte[], int tailleL) {
    int i;
    FILE* flot = fopen("adherent.txt","w");
    if (flot == NULL) {
        printf("Erreur d'ouverture du fichier !\n");
        fclose(flot);
        return -1;
    }
    for (i = 0 ; i < tailleL ; i++) {
        fprintf(flot,"%d %d %d %d %d %d\n",tabAdherent[i],tabPoints[i],jour[i],mois[i],annee[i],carte[i]);
    }
    fclose(flot);
    return 0;
}

int SauvegardeActivite(int tabCoutPoint[],int tabNbEntree[],int tailleL)
{
    int i;
    FILE* flot = fopen("activite.txt","w");
    if (flot == NULL) {
        printf("Erreur d'ouverture du fichier !\n");
        fclose(flot);
        return -1;
    }
    for (i = 0 ; i < tailleL ; i++) {
        if (tabCoutPoint[i] != 0)
            fprintf(flot,"%d %d %d\n",i, tabCoutPoint[i], tabNbEntree[i]);
    }
    fclose(flot);
    return 0;
}


int Sauvegarde(int tabAdhe[],int tabPoints[], int jour[], int mois[], int annee[], int carte[], int tabCoutPoint[], int tabNbEntree[], int tailleAdhe, int tailleAct) {
    int codeSauv, coderetourAdhe = -1, coderetourAct;
    printf("Sauvegarder ? (1 : Oui, 2 : Non)\n");
    scanf("%d",&codeSauv);
    if (codeSauv == 1) {

        while(coderetourAdhe == -1 || coderetourAct == -1) {
            coderetourAdhe = SauvegardeAdherent(tabAdhe,tabPoints, jour, mois, annee, carte, tailleAdhe);
            coderetourAct = SauvegardeActivite(tabCoutPoint,tabNbEntree,tailleAct);
            if (coderetourAdhe == 0 && coderetourAct == 0) {
                printf("Sauvegarde réussie !\n");
                break;
            }
            else {
                printf("Sauvegarde échouée\n");
                printf("Réessayer ?(1 : Oui, 2 : Non\n");
                scanf("%d",&codeSauv);
                if (codeSauv == 2) break;

            }        
        }
    }
    return 0;
}

int Menu(void){
    int select=0;

    while((select < 1 || select > 5) && select != 15 && select != 16 && (select < 6 || select > 10 ) && select != 20){
        printf("\t Gestion Salle de sport / Adhérent\n\n");
        printf("1\tAffichage des Adhérent\n");
        printf("2\tAjouter un Adhérent\n");
        printf("3\tSupprimer un Adhérent\n");
        printf("4\tGestion de la carte\n");
        printf("5\tParticiper à une activité\n");
        printf("\n\n20\tQuitter\n");
        printf("\n\nFaites '15' pour changer de page\n\n");
        printf("Choisir : ");
        scanf("%d",&select);

        if (select == 16)
        {
            printf("\t Gestion Salle de sport / Adhérent\n\n");
            printf("1\tAffichage des Adhérent\n");
            printf("2\tAjouter un Adhérent\n");
            printf("3\tSupprimer un Adhérent\n");
            printf("4\tGestion de la carte\n");
            printf("5\tParticiper à une activité\n");
            printf("\n\n20\tQuitter\n");
            printf("\n\nFaites '15' pour changer de page\n\n");
            printf("Choisir : ");
            scanf("%d",&select);
        }
        if(select==15){
            system("clear");
            printf("\t Gestion Salle de sport / Activité\n\n");
            printf("6\tInfos sur Activités globales\n");
            printf("7\tInformations sur une Activité\n");
            printf("8\tAjouter une Activité\n");
            printf("9\tSupprimer une Activité\n");
            printf("10\tModifier une Activité\n");
            printf("\n\n20\tQuitter\n");
            printf("\n\n16\tFaites '16' pour retourner a la page précédente\n\n");
            printf("Choisir : ");
            scanf("%d",&select);
        }
        if (select < 1|| select > 11 && select !=15 && select !=16 && select != 20)
            printf("Opération Impossible");
    }
    return select;
}

void GestionSalle(void){
    int tabAdhe[50]={0}, tabPoints[50]={0}, jour[50]={0}, mois[50]={0}, annee[50]={0}, carte[50]={0}, tailleLAdhe, tabCoutPoint[50]={0}, tabNbEntree[50]={0}, tailleLAct, select=0;
    tailleLAdhe=chargementAdherent(tabAdhe,tabPoints,jour,mois,annee,carte,50);
    tailleLAct=chargementActivite(tabCoutPoint, tabNbEntree, 50);
    if(tailleLAct < 0||tailleLAdhe < 0)
    {
        printf("tableau plein !\n");
        return;
    }
    while(select != 20){
        system("clear");
        select=Menu();
        system("clear");
        if(select == 1){
            affichageTous(tabAdhe,tabPoints,jour,mois,annee,carte,tailleLAdhe);
            clearpage();
        }
        if(select == 2){

            tailleLAdhe=ajouterAdherent(tabAdhe,tabPoints,jour,mois,annee,carte,tailleLAdhe,50);
            clearpage();
        }
        if(select == 3){
            tailleLAdhe=supprimerAdherent(tabAdhe,tabPoints,jour,mois,annee,carte,tailleLAdhe);
            clearpage();
        }
        if(select == 4){
            gestionCarte(tabAdhe,tabPoints,jour,mois,annee,carte,tailleLAdhe);
            clearpage();
        }
        if(select == 5){
            faireActivite(tabAdhe,tabPoints,tabCoutPoint,tabNbEntree,tailleLAdhe,tailleLAct);
            clearpage();
        }
        if(select == 6){
            affichageToutActivite(tabCoutPoint,tabNbEntree,tailleLAct);
            clearpage();
        }
        if(select == 7){
            affichageActivite(tabCoutPoint,tabNbEntree,tailleLAct);
            clearpage();
        }
        if(select == 8){
            ajoutActivite(tabCoutPoint,tailleLAct);
            clearpage();
        }
        if(select == 9){
            suppActivite(tabCoutPoint,tabNbEntree,tailleLAct);
            clearpage();
        }
        if(select == 10){
            modifActivite(tabCoutPoint,tailleLAct);
            clearpage();
        }
    }
    Sauvegarde(tabAdhe,tabPoints,jour,mois, annee,carte,tabCoutPoint, tabNbEntree, tailleLAdhe, tailleLAct);
}

void clearpage(void)
{
    char entre;
    printf("\nappuyé sur la touche [ENTREE] pour continuer");
    scanf("%*c%c", &entre);
    system("clear");
}

