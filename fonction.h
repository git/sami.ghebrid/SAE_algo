#include <stdio.h>
#include <stdlib.h>

int chargementAdherent(int tabAdherent[], int tabPoints[], int jour[], int mois[], int annee[], int carte[], int tphys);

int chargementActivite(int tabPoint[],int tabNbEntree[], int tphys);

int recherche(int tabTri[], int val, int nb, int *trouve);

void decalageDroite(int tabTri[], int nb, int pos);

void decalageGauche(int tabTri[], int nb, int pos);

int ajouter(int tabAdhe[], int tabPoints[], int jour[], int mois[], int annee[], int carte[], int nb, int val, int tmax);

int ajouterAdherent(int tabAdhe[], int tabPoints[], int jour[], int mois[], int annee[], int carte[], int nb, int tmax);

int suppression(int tab[], int tabPoints[], int jour[], int mois[], int annee[], int carte[],int nb, int val);

void affichageTous(int tabNumCarte[], int tabPoint[], int carte[], int jour[], int mois[], int annee[],int taille);

void affichageCarte(int tabAdhe[], int tabPoints[], int jour[], int mois[], int annee[], int carte[], int nb, int val);

void affichageActivite(int tabCoutPoint[], int tabNbEntree[], int tailleL);

void affichageToutActivite(int tabCoutPoint[], int tabNbEntree[], int tailleL);

void afficheNbEntreAct(int tabNbEntree[], int tabCoutPoint[], int tphys);

void alimenterCarte (int tabAdherent[], int tabPoint[],int nb);

void ajoutActivite(int tabCoutPoint[], int tailleL);

void suppActivite(int tabCoutPoint[], int tabNbEntree[],int tailleL);

void faireActivite(int tabAdherent[], int tabPoint[],int tabCoutPoint[], int tabNbEntree[], int tailleLAdhe, int tailleLAct);

int SauvegardeAdherent(int tabAdherent[], int tabPoints[], int jour[], int mois[], int annee[], int carte[], int tailleL);

int SauvegardeActivite(int tabCoutPoint[],int tabNbEntree[],int tailleL);

int supprimerAdherent(int tabAdhe[], int tabPoints[], int jour[], int mois[], int annee[], int carte[], int tailleL);

void modifActivite(int tabCoutPoint[],int tailleL);

int activerCarte(int tabAdhe[], int carte[], int tailleL, int val);

void gestionCarte(int tabAdhe[], int tabPoints[], int jour[], int mois[], int annee[], int carte[], int tailleL);

int Sauvegarde(int tabAdhe[],int tabPoints[], int jour[], int mois[], int annee[], int carte[], int tabCoutPoint[], int tabNbEntree[], int tailleAdhe, int tailleAct);

void GestionSalle(void);

int Menu(void);

void clearpage(void);